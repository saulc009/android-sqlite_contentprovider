# README #

This app uses ContentProvider to export the SQLite database stores in this app. With a predefined Uri, other applications who use the right Uri permission can access the database inside the app. This app uses all CRUD queries with ContentProvider. Works with app located in my other repository, link will be below.

### This app ONLY works with Android_SQLite_ContentProvider ###
* [SQLite_ContentProvider](https://bitbucket.org/saulc009/android-sqlite_contentprovider)

### Official Documentation ###

* [Content Provider](https://developer.android.com/reference/android/content/ContentProvider.html)