package creations.caridad.com.android_sqlite_contentprovider;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import creations.caridad.com.android_sqlite_contentprovider.fragments.FragmentAddNewUser;
import creations.caridad.com.android_sqlite_contentprovider.fragments.FragmentList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // call methods here
        setupListviewFragment();
    }

    private void setupListviewFragment() {
        FragmentList fragmentList = (FragmentList) getSupportFragmentManager().findFragmentByTag(FragmentList.TAG);
        if (fragmentList == null) {
            fragmentList = new FragmentList();
        }
        getSupportFragmentManager().beginTransaction().addToBackStack(FragmentList.TAG).replace(R.id.containerActivityMain, fragmentList, FragmentList.TAG).commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_activity_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // find menu action button
        switch (item.getItemId()) {
            case R.id.actionButtonActivityMainAdd:
                // start new fragment
                FragmentAddNewUser fragmentAddNewUser = (FragmentAddNewUser) getSupportFragmentManager().findFragmentByTag(FragmentAddNewUser.TAG);
                if (fragmentAddNewUser == null) {
                    fragmentAddNewUser = new FragmentAddNewUser();
                }
                getSupportFragmentManager().beginTransaction().addToBackStack(FragmentAddNewUser.TAG).replace(R.id.containerActivityMain, fragmentAddNewUser, FragmentAddNewUser.TAG).commit();
                break;
            default:
                return false;
        }
        return true;
    }
}
