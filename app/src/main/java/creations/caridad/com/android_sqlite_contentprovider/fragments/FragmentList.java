package creations.caridad.com.android_sqlite_contentprovider.fragments;


import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import creations.caridad.com.android_sqlite_contentprovider.CustomCursorAdapter;
import creations.caridad.com.android_sqlite_contentprovider.R;
import creations.caridad.com.android_sqlite_contentprovider.provider.MyContentProvider;
import creations.caridad.com.android_sqlite_contentprovider.provider.MyDataContract;


public class FragmentList extends ListFragment {

    public static final String TAG = "FragmentList";

    CustomCursorAdapter customCursorAdapter;

    // create interface
    public interface InterfaceListView {
        void passDataToNextActivity(int index);
    }

    // instance of interface
    private InterfaceListView interfaceListView;


    // attach interface
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof InterfaceListView) {
            interfaceListView = (InterfaceListView) context;
        } else {
            Log.i(TAG, "ERROR : Can't connect interface");
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // call methods here
        handleListView();
    }

    @Override
    public void onResume() {
        super.onResume();
        // anything that needs refreshing
        handleListView();
    }

    private void handleListView() {
        Cursor cursor = getCursor();
        customCursorAdapter = new CustomCursorAdapter(getContext(), cursor);
        // set method from SqLite_Helpers and set to listview
        setListAdapter(customCursorAdapter);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        // passing listview index to activityMain
        interfaceListView.passDataToNextActivity(position);
    }

    private void reloadData() {
        // will handle finding our cursor and setting it
        Cursor cursor = getCursor();
        customCursorAdapter.swapCursor(cursor);
        // will notify UI of data change
        customCursorAdapter.notifyDataSetChanged();
    }


    // find our cursor
    private Cursor getCursor() {
        Uri uri = Uri.parse(MyDataContract.DATASOURCEURI);
        return getActivity().getContentResolver().query(uri, MyDataContract.COLUMNS, null, null, null);
    }
}