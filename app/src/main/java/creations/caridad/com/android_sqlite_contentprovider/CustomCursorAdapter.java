package creations.caridad.com.android_sqlite_contentprovider;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.widget.CursorAdapter;
import android.widget.ResourceCursorAdapter;
import android.widget.TextView;

import creations.caridad.com.android_sqlite_contentprovider.provider.MyDataContract;

/**
 * Created by Saul on 6/10/17.
 */

public class CustomCursorAdapter extends ResourceCursorAdapter {

    public CustomCursorAdapter(Context context, Cursor cursor) {
        super(context, R.layout.custom_adapter_row, cursor, 0);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        // set XML ui
        TextView textViewFirstName = (TextView) view.findViewById(R.id.textUserName);
        textViewFirstName.setText(cursor.getString(cursor.getColumnIndex(MyDataContract.FirstName)));

        TextView textViewLastName = (TextView) view.findViewById(R.id.textUserAge);
        textViewLastName.setText(cursor.getString(cursor.getColumnIndex(MyDataContract.Age)));
    }
}
