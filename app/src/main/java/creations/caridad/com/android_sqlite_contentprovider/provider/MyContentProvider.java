package creations.caridad.com.android_sqlite_contentprovider.provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import creations.caridad.com.android_sqlite_contentprovider.SQLite.DatabaseHelper;

/**
 * Created by Saul on 6/10/17.
 */

public class MyContentProvider extends ContentProvider {

    public MyContentProvider() {
    }

    SQLiteDatabase sqLiteDatabase;
    UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);


    @Override
    public boolean onCreate() {

        // find our SQLite DB
        sqLiteDatabase = new DatabaseHelper(getContext()).getWritableDatabase();

        uriMatcher.addURI(getContext().getApplicationContext().getPackageName(), MyDataContract.TABLENAME, 1);
        uriMatcher.addURI(getContext().getPackageName(), MyDataContract.TABLENAME + "/#", 2);

        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        return sqLiteDatabase.query(MyDataContract.TABLENAME, projection, selection, selectionArgs, null, null, sortOrder);
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues contentValues) {
        sqLiteDatabase.insert(MyDataContract.TABLENAME, null, contentValues);
        return uri;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        int match = uriMatcher.match(uri);
        // if uri matches our uri, allow to delete
        if (match == 1 || match == 2) {
            int id = sqLiteDatabase.delete(MyDataContract.TABLENAME, selection, selectionArgs);
            return id;
        }
        return -1;
    }

    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return sqLiteDatabase.update(MyDataContract.TABLENAME, values, selection, selectionArgs);
    }
}
