package creations.caridad.com.android_sqlite_contentprovider.provider;

/**
 * Created by Saul on 6/10/17.
 */

public class MyDataContract {

    public static final String CONTENTURI = "content://com.android_sqlite_contentprovider/";

    public static final String TABLENAME = "users";

    public static final String DATASOURCEURI = CONTENTURI + TABLENAME;

    // Table Properties
    public static final String FirstName = "firstname";
    public static final String LastName = "lastname";
    public static final String Age = "age";
    public static final String ID = "_id";

    // returns all columns
    public static final String[] COLUMNS = {
            ID, FirstName, LastName, Age
    };
}
