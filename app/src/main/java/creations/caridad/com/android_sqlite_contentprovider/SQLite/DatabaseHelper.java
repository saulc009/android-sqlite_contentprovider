package creations.caridad.com.android_sqlite_contentprovider.SQLite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import creations.caridad.com.android_sqlite_contentprovider.provider.MyDataContract;

/**
 * Created by Saul on 6/10/17.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String DATABASEFILE = "database.db";
    public static final int DATABASEVERSION = 1;


    private static final String CREATETABLE = "CREATE TABLE IF NOT EXISTS " + MyDataContract.TABLENAME + " (" + MyDataContract.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + MyDataContract.FirstName + " TEXT, " + MyDataContract.LastName + " TEXT, " + MyDataContract.Age + " TEXT)";

    // constructor
    public DatabaseHelper(Context context) {
        super(context, DATABASEFILE, null, DATABASEVERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // EXECUTE
        db.execSQL(CREATETABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // HANDLE UPDATE
    }
}
