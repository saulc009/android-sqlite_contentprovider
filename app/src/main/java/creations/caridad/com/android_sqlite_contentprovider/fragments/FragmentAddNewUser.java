package creations.caridad.com.android_sqlite_contentprovider.fragments;


import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import creations.caridad.com.android_sqlite_contentprovider.CustomCursorAdapter;
import creations.caridad.com.android_sqlite_contentprovider.R;
import creations.caridad.com.android_sqlite_contentprovider.provider.MyDataContract;


public class FragmentAddNewUser extends Fragment {

    public static final String TAG = "FragmentAddNewUser";

    CustomCursorAdapter customCursorAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_new_user, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Cursor cursor = getCursor();
        customCursorAdapter = new CustomCursorAdapter(getContext(), cursor);

        // call methods here
        saveNewUser();
    }

    private void saveNewUser() {
        // XML UI
        Button buttonSave = (Button) getActivity().findViewById(R.id.buttonFragmentAddSave);
        final EditText enterFirstName = (EditText) getActivity().findViewById(R.id.editTextFragmentAddFirstName);
        final EditText enterLastName = (EditText) getActivity().findViewById(R.id.editTextFragmentAddLastName);
        final EditText enterAgeName = (EditText) getActivity().findViewById(R.id.editTextFragmentAddage);



        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // check if edit text fields are empty
                if (enterFirstName.getText().toString().trim().length() != 0 && enterLastName.getText().toString().trim().length() != 0 && enterAgeName.getText().toString().trim().length() != 0) {
                    // put user values into a content
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(MyDataContract.FirstName, enterFirstName.getText().toString());
                    contentValues.put(MyDataContract.LastName, enterLastName.getText().toString());
                    contentValues.put(MyDataContract.Age, enterAgeName.getText().toString());

                    // set URI with our contract
                    Uri uri = Uri.parse(MyDataContract.DATASOURCEURI);

                    // insert into DB
                    getContext().getContentResolver().insert(uri, contentValues);

                    // refresh data
                    reloadData();
                    // send message
                    Toast.makeText(getContext(), "Save ")
                    // go back
                    getFragmentManager().popBackStack();
                } else {
                    Toast.makeText(getContext(), "Please fill everything out", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void reloadData( ){
        // will handle finding our cursor and setting it
        Cursor cursor = getCursor();
        customCursorAdapter.swapCursor(cursor);
        // will notify UI of data change
        customCursorAdapter.notifyDataSetChanged();
    }


    // find our cursor
    private Cursor getCursor() {
        Uri uri = Uri.parse(MyDataContract.DATASOURCEURI);
        return getContext().getContentResolver().query(uri, MyDataContract.COLUMNS, null, null, null);
    }
}
